import Vue from 'vue'
import VueRouter from 'vue-router'
import HomeView from '../views/HomeView.vue'
import $store from '@/store'
import { Message } from 'element-ui'

Vue.use(VueRouter)

const routes = [
  {
    path:'/',
    redirect:'/home/index',
  },
  {
    path: '/home',
    name: 'home',
    component: HomeView,
    children:[{
      path:'index',
      component:()=>import('../views/Index.vue'),
      meta:{
        breadcrumb:['BMD后台管理','首页']
      }
    },{
      path:'actor-list',
      component:()=>import('../views/actor/ActorList.vue'),
      meta:{
        breadcrumb:['演员管理','演员列表']
      }
    },{
      path:'actor-add',
      component:()=>import('../views/actor/ActorAdd.vue'),
      meta:{
        breadcrumb:['演员管理','新增演员']
      }
    },{
      path:'director-list',
      component:()=>import('../views/director/DirectorList.vue'),
      meta:{
        breadcrumb:['导演管理','导演列表']
      }
    },{
      path:'director-add',
      component:()=>import('../views/director/DirectorAdd.vue'),
      meta:{
        breadcrumb:['导演管理','新增导演']
      }
    },{
      path:'movie-add',
      component:()=>import('../views/movie/MovieAdd.vue'),
      meta:{
        breadcrumb:['电影管理','新增电影']
      }
    },{
      path:'movie-list',
      component:()=>import('../views/movie/MovieList.vue'),
      meta:{
        breadcrumb:['电影管理','电影列表']
      }
    },
    {
      path:'movie-update/:id',
      component:()=>import('../views/movie/MovieUpdate.vue'),
      meta:{
        breadcrumb:['电影管理',
        {text:'电影列表',to:'/home/movie-list'},
        '修改电影信息']
      }
    },{
      path:'cinema-list',
      component:()=>import('../views/cinema/CinemaList.vue'),
      meta:{
        breadcrumb:['影院管理','影院列表']
      }
    },{
      path:'cinema-add',
      component:()=>import('../views/cinema/CinemaAdd.vue'),
      meta:{
        breadcrumb:['影院管理','新增影院']
      }
    },{
      path:'cinema-room-list/:cinemaId',
      component:()=>import('../views/cinema/CinemaRoomList.vue'),
      meta:{
        breadcrumb:['影院管理','影院列表','放映厅列表']
      }
    },{
      path:'showingon-plan-add/:cinemaId/:roomId',
      component:()=>import('../views/cinema/ShowingonPlanAdd.vue'),
      meta:{
        breadcrumb:['影院管理',{text:'电影院列表',to:'/home/cinema-list'},{text:'放映厅列表',to:'/home/cinema-room-list'},'添加排片计划']
      }
    },{
      path:'showingon-plan-list/:roomId',
      component:()=>import('../views/cinema/ShowingonPlanList.vue'),
      meta:{
        breadcrumb:['影院管理','影院列表','放映厅列表','排片列表']
      }
    },{
      path:'seat-template/:roomId',
      component:()=>import('../views/cinema/CinemaRoomSeatTemplate.vue'),
      meta:{
        breadcrumb:['影院管理','影院列表','放映厅列表','配置座位模板']
      }
    },
  ]
  },
  {
    path: '/about',
    name: 'about',
    component: () => import('../views/AboutView.vue')
  },
  {
    path: '/user/login',
    name: '/user/login',
    component: () => import('../views/user/Login.vue')
  }
]

const router = new VueRouter({
  mode: 'hash',
  base: process.env.BASE_URL,
  routes
})

// 为router路由管理器注册全局前置守卫，
// 一旦有路由跳转行为发生，则跳转到目标组件之前执行这个回调方法
router.beforeEach((to,from,next)=>{
  console.log({to,from,next});
  if (to.path=='/user/login'|| $store.state.user) {
    // 手动调用next方法
    next()
  }else{
    router.push('/user/login')
    Message.warning('暂无登录状态，请重新登录')
  }
})


export default router
