import httpApi from '@/HTTP'
import Vue from 'vue'
import Vuex from 'vuex'
import $router from '@/router';
import { Message } from "element-ui";
import { keys,storage } from '@/utils/storage';

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    user:storage.get(keys.CURRENT_USER), //保存当前用户数据
    token:storage.get(keys.TOKEN),//保存token
  },
  getters: {
  },
  mutations: {
    //保存token字符串
    saveToken(state,payload){
      state.token = payload
      storage.set(keys.TOKEN,payload)

    },

    //声明方法，修改state中的user用户信息，传入用户对象
    updateUser(state,payload){
      state.user = payload
      // 将user对象，存入sessionStorage
      storage.set(keys.CURRENT_USER,payload)
    }
  },
  actions: {
    loginAction(store,payload){
      httpApi.adminApi.login(payload).then(res=>{
        if (res.data.code==200) {
          let user = res.data.data.user
          store.commit('updateUser',user)
          store.commit('saveToken',res.data.data.token)


          Message.success('登录成功')
          $router.push('/')
        }else{
          Message.error('登录失败:'+res.data.msg)
        }
      })
    }
  },
  modules: {
  }
})
