// WebStorage存储工具

export const keys = {
  CURRENT_USER :'CURRENT_USER',
  // CITY:CITY
  TOKEN:'TOKEN'
}
export const storage = {
  set(key,value){
    sessionStorage.setItem(key,JSON.stringify(value))
  },
  get(key){
    let value = sessionStorage.getItem(key)
    return JSON.parse(value)
  }
}