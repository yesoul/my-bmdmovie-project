import myaxios from "./MyAxios"
import actorApi from "./apis/ActorApi"
import directorApi from "./apis/DirectorApi";
import movieApi from './apis/MovieApi'
import cinemaApi from './apis/CinemaApi'
import showingonPlanApi from "./apis/ShowingonPlanApi";
import cinemaRoomApi from "./apis/CinemaRoomApi";
import adminApi from "./apis/AdminApi"

const httpApi = {
  actorApi,
  directorApi,
  movieApi,
  cinemaApi,
  cinemaRoomApi,
  showingonPlanApi,
  adminApi
}

export default httpApi;