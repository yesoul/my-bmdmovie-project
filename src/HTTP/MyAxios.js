import axios from 'axios'
import qs from 'qs'
import $store from '@/store'
import $router from '@/router';
import { Message } from 'element-ui';

const instance = axios.create() //创建axios实例


// 为instance注册请求拦截器
instance.interceptors.request.use((config)=>{
    console.log('执行了请求拦截器',config);
    config.headers['Authorization'] = $store.state.token
    return config;
})

//编写axios的响应拦截器，拦截所有响应，做统一业务异常处理
instance.interceptors.response.use(res=>{
    //res就是axios相应数据对象
    if(res.data.code == 401){
        $router.push('/user/login')
        Message.warning('账号已失效，请重新登录')
    }
    return res;
})

const myaxios = {
    get(url,params){
        return instance({
            method:'GET',
            url,
            params,
        })
    },

    post(url,params){
        return instance({
            method:'POST',
            url,
            data:qs.stringify(params)
        })
    }
}

export default myaxios