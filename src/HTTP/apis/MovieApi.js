
import myaxios  from "../MyAxios";
import BaseUrl from '../BaseUrl';

const BMDURL = BaseUrl.BMDURL

const movieApi = {
  /**
   * 通过id查询电影信息
   * @param {Object} params {id：1}
   */
  queryById(params){
    let url = BMDURL + '/movie-info/query'
    return myaxios.get(url,params)
  },


  /**
   * 修改电影信息
   * @param {Object} params 电影对象
   */
  update(params){
    let url = BMDURL + '/movie-info/update'
    return myaxios.post(url,params)
  },
  

  /**
   * 查询电影类型
   */
  queryTypes(){
    let url = BMDURL + '/movie-types'
    return myaxios.get(url)
  },



  /**
   * 删除电影
   * @param {Object} params {id ：1}
   */
  delete(params){
    let url = BMDURL + '/movie-info/del'
    return myaxios.post(url,params)
  },

  /**
   * 新增电影
   * @param {Object} params {movieName:xx, movieAvatar:xx}
   */
  add(params){
    let url= BMDURL + '/movie-info/add'
    return myaxios.post(url,params)
  },

  /**
   * GET查询全体电影列表
   */
  queryAllMovies(params){
    let url = BMDURL + '/movie-infos'
    return myaxios.get(url,params)
  },
  
  /**
   * 通过关键字查询电影列表
   * @param {Object} params 请求参数 数据格式： { name:'关键字' }
   */
  queryMoviesByName(params){
    let url = BMDURL + '/movie-infos/name'
    return myaxios.post(url,params)
  }
}

export default movieApi