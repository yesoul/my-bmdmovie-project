
import myaxios  from "../MyAxios";
import BaseUrl from '../BaseUrl';

const BMDURL = BaseUrl.BMDURL

const actorApi = {

  /**
   * 查询所有影院标签
   */
  queryTypes(){
    let url = BMDURL + '/cinema/tags'
    return myaxios.get(url)
  },
  
    /**
   * 新增影院标签
   * @param {Object} params 影院参数对象
   */
    add(params){
      let url = BMDURL + '/cinema/add'
      return myaxios.post(url,params)
    },

    /**
   * 查询所有影院
   */
  queryAll(){
    let url = BMDURL + '/cinemas'
    return myaxios.get(url)
  },
}

export default actorApi