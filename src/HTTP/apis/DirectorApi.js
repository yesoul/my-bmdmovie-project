
import myaxios  from "../MyAxios";
import BaseUrl from '../BaseUrl';

const BMDURL = BaseUrl.BMDURL

const directorApi = {

  /**
   * 删除导演
   * @param {Object} params {id ：1}
   */
  delete(params){
    let url = BMDURL + '/movie-directors/del'
    return myaxios.post(url,params)
  },

  /**
   * 新增导演
   * @param {Object} params {directorName:xx, directorAvatar:xx}
   */
  add(params){
    let url= BMDURL + '/movie-directors/add'
    return myaxios.post(url,params)
  },

  /**
   * GET查询全体导演列表
   */
  queryAllDirectors(){
    let url = BMDURL + '/movie-directors'
    let params = {page:1,pagesize:100}
    return myaxios.get(url,params)
  },
  
  /**
   * 通过关键字查询导演列表
   * @param {Object} params 请求参数 数据格式： { name:'关键字' }
   */
  queryDirectorsByName(params){
    let url = BMDURL + '/movie-directors/name'
    return myaxios.post(url,params)
  }
}

export default directorApi