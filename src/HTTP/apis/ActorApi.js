
import myaxios  from "../MyAxios";
import BaseUrl from '../BaseUrl';

const BMDURL = BaseUrl.BMDURL

const actorApi = {

  /**
   * 删除演员
   * @param {Object} params {id ：1}
   */
  delete(params){
    let url = BMDURL + '/movie-actors/del'
    return myaxios.post(url,params)
  },

  /**
   * 新增演员
   * @param {Object} params {actorName:xx, actorAvatar:xx}
   */
  add(params){
    let url= BMDURL + '/movie-actors/add'
    return myaxios.post(url,params)
  },

  /**
   * GET查询全体演员列表
   */
  queryAllActors(){
    let url = BMDURL + '/movie-actors'
    let params = {page:1,pagesize:100}
    return myaxios.get(url,params)
  },
  
  /**
   * 通过关键字查询演员列表
   * @param {Object} params 请求参数 数据格式： { name:'关键字' }
   */
  queryActorsByName(params){
    let url = BMDURL + '/movie-actors/name'
    return myaxios.post(url,params)
  }
}

export default actorApi