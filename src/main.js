import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'


//引入elementUI
import ElementUI from "element-ui";
import 'element-ui/lib/theme-chalk/index.css'

Vue.use(ElementUI);

import BaseUrl from './HTTP/BaseUrl';
Vue.prototype.$UPLOADURL = BaseUrl.UPLOADURL

Vue.prototype.$eventBus = new Vue()

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
